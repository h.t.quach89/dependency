package com.mhp.coding.challenges.dependency.inquiry.interfaces;

import com.mhp.coding.challenges.dependency.inquiry.Inquiry;

public interface PushNotificationHandlerInterface {
    public void sendNotification(final Inquiry inquiry);
}
