package com.mhp.coding.challenges.dependency.inquiry;

import com.mhp.coding.challenges.dependency.inquiry.interfaces.EmailHandlerInterface;
import com.mhp.coding.challenges.dependency.inquiry.interfaces.PushNotificationHandlerInterface;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class InquiryService {

    private static final Logger LOG = LoggerFactory.getLogger(InquiryService.class);
    private final EmailHandlerInterface emailHandlerInterface;
    private final PushNotificationHandlerInterface pushNotificationHandlerInterface;

    public void create(final Inquiry inquiry) {
        emailHandlerInterface.sendEmail(inquiry);
        pushNotificationHandlerInterface.sendNotification(inquiry);
        LOG.info("User sent inquiry: {}", inquiry);
    }

}
