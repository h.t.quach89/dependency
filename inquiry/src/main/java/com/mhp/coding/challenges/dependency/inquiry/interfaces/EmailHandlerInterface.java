package com.mhp.coding.challenges.dependency.inquiry.interfaces;

import com.mhp.coding.challenges.dependency.inquiry.Inquiry;

public interface EmailHandlerInterface {
    public void sendEmail(final Inquiry inquiry) ;
}
